import bpy
from math import radians
import numpy as np
from scipy.spatial import KDTree
import os
import matplotlib.pyplot as plt


def principal_axis_align(meshes):
    '''
    Rotate the scanned mesh to align its principal axis with the original mesh's principal axis
    This presently is done off of vertices, but should ideally be done off of faces
    '''
    # convert all of our meshes to numpy arrays
    scanned_points = np.array([meshes[0].matrix_world @ v.co for v in meshes[0].data.vertices])
    original_points = np.array([meshes[1].matrix_world @ v.co for v in meshes[1].data.vertices])

    def find_principal_axis(points):
        '''
        A sub function for finding the principal axis
        Returns a unit vector representing the principal axis of the given points (numpy array)
        '''
        # Find the covariance matrix
        cov = np.cov(points.T)
        # Find the eigenvalues and eigenvectors of the covariance matrix
        eigvals, eigvecs = np.linalg.eig(cov)
        # Find the index of the largest eigenvalue
        max_eigval_index = np.argmax(eigvals)
        # Find the corresponding eigenvector
        max_eigvec = eigvecs[:, max_eigval_index]
        # Convert to a unit vector (check if this is done automatically)
        return max_eigvec / np.linalg.norm(max_eigvec)
    
    # Find the principal axis of the scanned and orginal meshes
    scanned_principal_axis = find_principal_axis(scanned_points)
    original_principal_axis = find_principal_axis(original_points)

    # Now we can calculate the rotation matrix by applying the Rodrigues rotation formula
    # https://mathworld.wolfram.com/RodriguesRotationFormula.html
    # Find the angle between the principal axis of the scanned object and the principal axis of the original object
    angle = np.arccos(np.dot(scanned_principal_axis, original_principal_axis))
    # Find the axis of rotation between the two principal axes by taking the cross product
    axis = np.cross(scanned_principal_axis, original_principal_axis)
    x, y, z = axis # unpack the axis of rotation
    c = np.cos(angle) # cos(angle)
    s = np.sin(angle) # sin(angle)
    t = 1 - c # cos complement
    rotation_matrix = np.array([
        [t*x*x + c, t*x*y - z*s, t*x*z + y*s],
        [t*x*y + z*s, t*y*y + c, t*y*z - x*s],
        [t*x*z - y*s, t*y*z + x*s, t*z*z + c]
    ])
    
    # Here we directly change the coords of each vertex in the actual scanned mesh by applying the rotation matrix (this was easier than applying it as an actual blender rotation)
    for v in meshes[0].data.vertices:
        v.co = rotation_matrix @ v.co


def scale_model(meshes, target_axis='none'):
    '''
    After the scanned and original meshes are aligned, find the percent difference between the width, height, and depth of the two meshes
    Then scale the scanned mesh by that percent difference
    Takes an optional target_axis parameter to specify which axis to scale the scanned mesh by
    '''
    # Create a numpy array of the vertices of the selected object in absolute coordinates
    scanned_points = np.array([meshes[1].matrix_world @ v.co for v in meshes[0].data.vertices])
    original_points = np.array([meshes[0].matrix_world @ v.co for v in meshes[1].data.vertices])
    scanned_size, scanned_dim = scanned_points.shape
    original_size, orignal_dim = original_points.shape

    # Find the width, height, and depth of the scanned and original meshes
    scanned_width = np.max(scanned_points[:, 0]) - np.min(scanned_points[:, 0])
    scanned_height = np.max(scanned_points[:, 1]) - np.min(scanned_points[:, 1])
    scanned_depth = np.max(scanned_points[:, 2]) - np.min(scanned_points[:, 2])
    original_width = np.max(original_points[:, 0]) - np.min(original_points[:, 0])
    original_height = np.max(original_points[:, 1]) - np.min(original_points[:, 1])
    original_depth = np.max(original_points[:, 2]) - np.min(original_points[:, 2])
    print(f'X difference (mm): {scanned_width - original_width}')
    print(f'Y difference (mm): {scanned_height - original_height}')
    print(f'Z difference (mm): {scanned_depth - original_depth}')
    # Find the percent difference between the scanned and original meshes
    width_percent = scanned_width / original_width
    height_percent = scanned_height / original_height
    depth_percent = scanned_depth / original_depth
    print(f'X percent: {width_percent*100}')
    print(f'Y percent: {height_percent*100}')
    print(f'Z percent: {depth_percent*100}')

    # Scale the scanned mesh uniformly by the specified target_axis
    scale_factor = 1
    if target_axis == 'X' or target_axis == 'x':
        scale_factor = width_percent
    elif target_axis == 'Y' or target_axis == 'y':
        scale_factor = height_percent
    elif target_axis == 'Z' or target_axis == 'z':
        scale_factor = depth_percent

    # Scale the scanned mesh by the percent difference
    for v in meshes[1].data.vertices:
        v.co *= scale_factor


def icp_step(meshes, max_iterations, tolerance=0.01):
    '''
    Run Iterative Closest Point (ICP) to translate and rotate the scanned (first) mesh to align with the original (second) mesh
    https://www.ipb.uni-bonn.de/html/teaching/msr2-2020/sse2-03-icp.pdf
    Immediately converts to numpy arrays and performs all calculations in absolute coordinates
    '''
    # Create a numpy array of the vertices of the selected object in absolute coordinates
    scanned_points = np.array([meshes[1].matrix_world @ v.co for v in meshes[0].data.vertices])
    original_points = np.array([meshes[0].matrix_world @ v.co for v in meshes[1].data.vertices])
    scanned_size, scanned_dim = scanned_points.shape
    original_size, orignal_dim = original_points.shape
    
    transformation = np.identity(scanned_dim)
    translation_final = np.zeros(scanned_dim)
    rotations_final = np.identity(scanned_dim)
    
    for i in range(max_iterations):
        # Find the nearest neighbors between the scanned and original points
        tree = KDTree(original_points)
        distances, indices = tree.query(scanned_points)

        # Compute the transformation matrix using singular value decomposition
        scanned_mean = np.mean(scanned_points, axis=0)
        original_mean = np.mean(original_points[indices], axis=0)
        centered_scanned = scanned_points - scanned_mean # subtract the mean from each point
        centered_original = original_points[indices] - original_mean 

        covariance_matrix = np.dot(centered_scanned.T, centered_original)
        u, _, vh = np.linalg.svd(covariance_matrix)
        rotation = np.dot(u, vh)
        translation = original_mean - np.dot(rotation, scanned_mean)

        # Update the transformation matrix (and our translation and rotations)
        transformation = np.dot(rotation, transformation)
        transformation[:, -1] += translation
        translation_final += translation
        rotations_final = np.dot(rotation, rotations_final)

        # Apply the transformation to the source points, just dot with the rotation matrix and add the translation vector
        transformed_source = np.dot(scanned_points, rotation) + translation
        scanned_points = transformed_source
        
        # Check the convergence criteria
        # Here I should really be taking into account the actual vector connecting points such that direction is accounted for
        error = np.mean(distances)
        if error < tolerance:
            break

    # For each vertex in the actual scanned mesh object, move it to the corresponding vertex in the new_points array
    for i, v in enumerate(meshes[0].data.vertices):
        v.co = scanned_points[i]


def calculate_error(meshes):
    '''
    Find the distance between each point on the original mesh and the closest face on the scanned mesh
    '''
    # Create a numpy array of the vertices of both objects in absolute coordinates
    scanned_points = np.array([meshes[1].matrix_world @ v.co for v in meshes[0].data.vertices])
    original_points = np.array([meshes[0].matrix_world @ v.co for v in meshes[1].data.vertices])

    # Find the distance between all points on the original mesh and the closest face on the scanned mesh
    tree = KDTree(scanned_points)
    distances, indices = tree.query(original_points)

    # Initialize our distance array
    errors = np.zeros(len(original_points))

    # now find the closest point on the target mesh to each point on the source mesh
    for i, point in enumerate(original_points):
        # find the closest point on the scanned mesh to each point on the original mesh
        errors[i] = distances[i]

    name = bpy.context.object.name
    outputFile  = bpy.path.abspath("//csv_export\\" + name + ".csv")
    # save errors to a csv in the local directory
    np.savetxt(outputFile, errors, delimiter=",")

    return errors


def assign_colors(meshes, errors):
    '''
    Assign colors to each face of the original mesh based on the error at each vertex
    https://medium.com/@bldevries/using-python-and-custom-data-to-vertex-color-your-blender-model-4fd0d69134a3
    '''
    # We want to color the original mesh
    original_points = meshes[1]

    # delete all previous vertex color layers
    for i in range(len(original_points.data.vertex_colors)):
        original_points.data.vertex_colors.remove(original_points.data.vertex_colors[0])

    # Find the max and min errors
    max_error = np.max(errors)
    min_error = np.min(errors)
    print(f'Max error: {max_error}')

    # Normalize the errors
    errors_norm = (errors - min_error) / (max_error - min_error)

    # Create a new vertex color layer
    color_layer = original_points.data.vertex_colors.new(name="Color Errors")

    # We assign some amount of red based on error_norm
    # Because each vertex has a value for each face it's connected to, we need to iterate through each vertex as well
    for poly in original_points.data.polygons:
        for vert_i_poly, vert_i_mesh in zip(poly.loop_indices, poly.vertices):
            # Assign the color to the vertex
            color_layer.data[vert_i_poly].color = (errors_norm[vert_i_mesh], 0, 0, 1)


def slice_rotors(meshes, slice_location_percent, slice_axis = 'y', slice_width = 0.1):
    '''
    Produces 2D cross section segment of all the points that fall within the bounds created by
    dividing the scanned and original meshes into vertical sliced number of divisions,
    '''
    # Create a numpy array of the vertices of both objects in absolute coordinates
    scanned_points = np.array([meshes[1].matrix_world @ v.co for v in meshes[0].data.vertices])
    original_points = np.array([meshes[0].matrix_world @ v.co for v in meshes[1].data.vertices])

    # Find the min and max of the scanned mesh along the y-axis
    scanned_min = np.min(scanned_points[:,1])
    scanned_max = np.max(scanned_points[:,1])
    # Same for original mesh
    original_min = np.min(original_points[:,1])
    original_max = np.max(original_points[:,1])
    # Take the larger of the two minima and the smaller of the two maxima
    min_y = np.max([scanned_min, original_min])
    max_y = np.min([scanned_max, original_max])

    # Find the y-value of the slice
    slice_y = min_y + (max_y - min_y) * slice_location_percent

    # Create an array to hold the points that fall within the slice
    scanned_sliced_points = []
    original_sliced_points = []

    # Find the points that fall within the scanned slice
    for point in scanned_points:
        if point[1] >= slice_y - slice_width/2 and point[1] <= slice_y + slice_width/2:
            scanned_sliced_points.append(point)
    # Same for original slice
    for point in original_points:
        if point[1] >= slice_y - slice_width/2 and point[1] <= slice_y + slice_width/2:
            original_sliced_points.append(point)
    # Quickly convert to numpy arrays
    scanned_sliced_points = np.array(scanned_sliced_points)
    original_sliced_points = np.array(original_sliced_points)

    # get the max and min x and z values of the scanned points
    scanned_x_min = np.min(scanned_sliced_points[:,0])
    scanned_x_max = np.max(scanned_sliced_points[:,0])
    box_bound = (scanned_x_max - scanned_x_min)*1.25
    # Add a cube to the scene to represent the slice width
    bpy.ops.mesh.primitive_cube_add(size=1, enter_editmode=False, align='WORLD', location=(0, slice_y, 0), scale=(box_bound, slice_width, box_bound))
    # Get the cube object
    cube = bpy.context.object
    # Set the cube to be wireframe
    cube.display_type = 'WIRE'
 
    # set the image resolution in pixels
    plt.gcf().set_size_inches(1080/100, 1080/100)
    plt.scatter(original_sliced_points[:,0], original_sliced_points[:,2], s=0.1, c='red', label='Original')
    plt.scatter(scanned_sliced_points[:,0], scanned_sliced_points[:,2], s=0.1, c='blue', label='Scanned', alpha=0.5)
    plt.xlim(scanned_x_min*1.1, scanned_x_max*1.1)
    plt.ylim(scanned_x_min*1.1, scanned_x_max*1.1)
    plt.gca().set_aspect('equal', adjustable='box') # Set axes to be equal scale
    # save the plot
    name = 'slice_' + str(slice_location_percent)
    outputFile  = bpy.path.abspath("//csv_export\\" + name + ".png")
    plt.savefig(outputFile)


def main():
    # For each mesh, set the origin to the center of mass and move it to the global origin
    meshes = []
    for so in bpy.data.objects:
        # if the object is not the camera, or the lamp
        if so.type != 'CAMERA' and so.type != 'LAMP':
            # Set the mesh origin to its center of mass and move it to the global origin
            bpy.ops.object.select_all(action='DESELECT')
            so.select_set(True)
            # Set the center of mass to be the mesh's origin
            bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_VOLUME')
            so.location = [0,0,0]
            # permanently apply the transformation
            bpy.ops.object.transform_apply(location=True, rotation=True, scale=True)
            # Add the mesh to the list of meshes
            meshes.append(so)
    ''' Align the principal axis of each mesh with the z axis '''
    #principal_axis_align(meshes)

    ''' Scale the meshes if there's a scaling issue) '''
#    scale_model(meshes)
#    scale_model(meshes, 'y')

    ''' Perform ICP to finish the alignment for non-radially symmetric parts '''
#    icp_step(meshes, 5)

    '''Calculate the error between the two meshes. The assign colors to the original mesh based on the error at each point '''
#    errors = calculate_error(meshes)
#    assign_colors(meshes, errors)

    ''' Slice the meshes into 2D cross sections '''
    slice_rotors(meshes, 0.5, 'y', 650 * 0.005)

if __name__ == "__main__":
   main()