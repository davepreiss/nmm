from math import *
import random
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import time
from numba import jit
import subprocess


def velocity_magnitude(dx,dy):
    return np.hypot(dx, dy) #    return (dx**2 + dy**2)**0.5

def calc_energy(sides, dms):
    ''' Calculate the energy of the system by summing the kinetic energy of the particles on each side.'''
    on_left = len(np.where(sides == 0)[0])
    on_right = num_particles - on_left
    return np.sum(dms[np.where(sides == 0)])/on_left, np.sum(dms[np.where(sides == 1)])/on_right

# @jit(nopython=True)
def time_step(time, xs, ys, dxs, dys, dms, sides, energy):
    ''' Time_step accepts pointers to the whole array, but will frequently use [time,:] or [time-1,:]
        to access all the current or previous values.
    '''
    sides[time-1,:] = np.where(xs[time-1,:] < width/2, 0, 1) # 0 is left (hot) and 1 is right (cold)
    left_bound  = np.where(sides[time-1] == 0, 0, width/2)          # if we are on the left side, left bound is 0, if we are on the right side it's width/2
    right_bound = np.where(sides[time-1] == 1, width, width/2)      # if we are on the right side, right bound is width, if we are on the left it's width/2
    xs[time,:] = xs[time-1,:] + dxs[time-1,:] * dt # update the current times from previous velocities
    ys[time,:] = ys[time-1,:] + dys[time-1,:] * dt

    # If we are a hot particle on the right side, open the door to pass left (first two lines check if we are door height)
    left_bound = np.where(    (height/2-door_height/2 <= ys[time,:])\
                            & (ys[time,:] <= height/2 + door_height/2)\
                            & (dms[0,:] > velocity_max/2)\
                            & (sides[time-1,:] == 1),\
                            0, left_bound) # if we are a hot particle on the right side we pass to the left
    # If we are a cold particle on the left side open the door to pass right
    right_bound = np.where(   (height/2-door_height/2 <= ys[time,:])\
                            & (ys[time,:] <= height/2 + door_height/2)\
                            & (dms[0,:] < velocity_max/2)\
                            & (sides[time-1,:] == 0),\
                            width, right_bound) # if we are a cold particle on the left side we pass to the right

    # Check if we are bouncing off of a boundary and update velocity and position in y then x if so
    dys[time,:] = np.where((ys[time,:] < 0) | (ys[time,:] > height), -dys[time-1,:], dys[time-1,:]) # if we are outside in -y or +y, reverse the velocity
    ys[time,:] =  np.where((ys[time,:] < 0) | (ys[time,:] > height), ys[time,:] + dt*dys[time,:], ys[time,:]) # then update the position to be bounced with the new velocity
    dxs[time,:] = np.where((xs[time,:] < left_bound) | (xs[time,:] > right_bound), -1*dxs[time-1,:], dxs[time-1,:]) # if we are outside in -x or +x, reverse the velocity
    xs[time,:]  = np.where((xs[time,:] < left_bound) | (xs[time,:] > right_bound),  xs[time,:] + dt*dxs[time,:], xs[time,:]) # same for x
    energy[time,:] = calc_energy(sides[time-1,:], dms[0,:])

def run_sim():
    # initialize numpy arrays of x,y,dx,dy for each particle - arrays have a column for every particle and a row for every time step
    times = np.arange(0,t_final,dt)
    xs = np.zeros((int(t_final/dt),num_particles))
    ys = np.zeros((int(t_final/dt),num_particles))
    dxs = np.zeros((int(t_final/dt),num_particles))
    dys = np.zeros((int(t_final/dt),num_particles))
    dms = np.zeros((int(t_final/dt),num_particles)) # velocity magnitude
    sides = np.zeros((int(t_final/dt),num_particles)) # 0 is left (hot) and 1 is right (cold)
    # these two are constant for each particle and so just need two or one rows respectively
    energy = np.zeros((int(t_final/dt), 2)) # total energy of particles on the left (0) and right (1) side
    colors = np.zeros(num_particles) # RGB colors which are constant for each particle

    # Assign the first row of each array (t=0) to a random or calculated value
    xs[0,:] = np.random.uniform(0,width,num_particles) 
    ys[0,:] = np.random.uniform(0,height,num_particles)
    dxs[0,:] = np.random.uniform(-velocity_max*.707,velocity_max*.707,num_particles)
    dys[0,:] = np.random.uniform(-velocity_max*.707,velocity_max*.707,num_particles)
    dms[0,:] = velocity_magnitude(dxs[0,:],dys[0,:])
    sides[0,:] = np.where(xs[0,:] < width/2, 0, 1)
    energy[0,:] = calc_energy(sides[0,:], dms[0,:])
    colors = np.where(dms[0,:] < velocity_max/2, 0, 1) # 0 is hot, 1 is cold

    # Update each time step
    for t in range(1,int(t_final/dt)):
        time_step(t, xs, ys, dxs, dys, dms, sides, energy)

    return xs, ys, colors, energy


# Simulation Constants
num_particles = 5000
width = 1000
height = 500
door_height = height/2
dt = 0.06 # time interval in seconds
t_final = 50
velocity_max = 100
time1 = time.time()
xs, ys, colors, energy = run_sim() # run the simulation
solve_time = time.time() - time1

# Plotting the Simualtion
my_dpi = 96
fig = plt.figure(figsize=(800/my_dpi, 400/my_dpi), dpi=my_dpi)
ax = fig.add_axes([0, 0, 1, 1], frameon=False)
ax.set_xlim(0,width), ax.set_xticks([])
ax.set_ylim(0,height), ax.set_yticks([])
# Hot side label and energy
plt.text(10, 480, 'Hot Side', fontdict=None)
hot_text = plt.text(10, 465, f'{energy[0][0]: .2f}', fontdict=None)
# Cold Side label and energy
plt.text(510, 480, 'Cold Side', fontdict=None)
cold_text = plt.text(510, 465, f'{energy[0][1]: .2f}', fontdict=None)
# Total time to simulate
plt.text(10, 10, f'Total Time to calculate: {solve_time: .4f} s', fontdict=None)
plt.plot([width/2, width/2], [0, height/2-door_height/2], color = 'black')
plt.plot([width/2, width/2], [height, height/2+door_height/2], color = 'black')
plt.plot([width/2, width/2], [height/2+door_height/2, height/2-door_height/2], color = 'yellow')
plot = ax.scatter(xs[0,:], y = ys[0,:], # initial positions in X and Y
                  marker = 'o',
                  s = 10,
                  color = np.where(colors == 0, 'b', 'r'))

def animate(w):
    # In animate you ideally just want to be updating positions / size / colors etc and not re-plotting everything with clear.
    x = xs[w,:]
    y = ys[w,:]
    plot.set_offsets(np.c_[x,y])
    hot_text.set_text(f'Avg. Velocity: {energy[w,0]: .2f}')
    cold_text.set_text(f'Avg. Velocity: {energy[w,1]: .2f}')

anim = animation.FuncAnimation(fig, animate, frames=int(t_final/dt), blit=False, repeat=False, interval=1/30*1000)
# plt.show()

f = r"c://Users/david/Desktop/NMM/week1/img/animation_fast.gif" 
writervideo = animation.FFMpegWriter(fps=30, extra_args=['-vcodec', 'libx264']) 
# anim.save(f, writer=writervideo, dpi=30)
anim.save(f, writer='imagemagick', fps=30, dpi=my_dpi)