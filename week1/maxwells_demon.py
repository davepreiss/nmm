from matplotlib import rc
# rc('animation', html='jshtml')

from math import *
import random
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import time
from numba import jit

class Particle:
    def __init__(self): # initialize X and Y to zero if none is given
        self.x = random.uniform(0, width)
        self.y = random.uniform(0, height)
        self.dx = random.uniform(-velocity_max*.707, velocity_max*.707)
        self.dy = random.uniform(-velocity_max*.707, velocity_max*.707)
        self.dm = (self.dx**2 + self.dy**2)**0.5 # velocity magnitude
        self.color = np.array([0, 0, 1]) if self.dm < velocity_max/2 else np.array([1, 0, 0])
    def move(self):
        self.side = 0 if self.x < width/2 else 1 # 0 is left (hot) and 1 is right (cold)
        self.left_bound = 0 if self.side == 0 else width/2
        self.right_bound = width if self.side == 1 else width/2
        self.x_new = self.x + self.dx * dt
        self.y_new = self.y + self.dy * dt
        # check if the door is open
        if height/2-door_height/2 <= self.y_new <= height/2+door_height/2: # if we are at door height
            if self.dm > velocity_max/2 and self.side == 1: # if we are a hot particle on the right side we pass
                self.left_bound = 0
            if self.dm < velocity_max/2 and self.side == 0: # if we are a cold particle on the left side we pass
                self.right_bound = width
        # check if we are bouncing off of a boundary
        if not 0 <= self.y_new <= height:
            self.dy = -self.dy
            self.y_new = self.y + self.dy*dt
        if not self.left_bound <= self.x_new <= self.right_bound:
            self.dx = -self.dx
            self.x_new = self.x + self.dx*dt

        self.x = self.x_new
        self.y = self.y_new
    def update_state(self,t):
        self.states[int(t/dt),:] = [self.x,self.y,self.dx,self.dy]
    def increment(self,t):
        self.move()
        # self.update_state(t)

# Simulation code
num_particles = 100
width = 1000
height = 500
door_height = height/2
dt = 0.1 # time interval in seconds
t_final = 200
velocity_max = 50

fig = plt.figure(figsize=(10,5))
ax = plt.axes(xlim=(0,width), ylim=(0,height))

# Here we will just run the whole thing once to get the run time
time1 = time.time()
particles = []
for _ in range(num_particles):
    particles.append(Particle())
xs = []
ys = []
colors = []
for s in range(int(t_final/dt)):
    for p in particles:
        p.increment(s*dt)
        xs.append(p.x)
        ys.append(p.y)
        colors.append(p.color)
run_time = time.time() - time1
print("Run time: " + str(run_time))

# Now we will animate it
def frame(w):
    ax.clear()
    xs = []
    ys = []
    colors = []
    for p in particles:
        p.increment(w)
        xs.append(p.x)
        ys.append(p.y)
        colors.append(p.color)
    plt.title("Maxwell's Demon")
    ax.set_xlim(0,width)
    ax.set_ylim(0,height)
    plt.plot([width/2, width/2], [0, height/2-door_height/2], color = 'black')
    plt.plot([width/2, width/2], [height, height/2+door_height/2], color = 'black')
    plt.plot([width/2, width/2], [height/2+door_height/2, height/2-door_height/2], color = 'yellow')
    plot = ax.scatter(xs, ys, c=colors, marker='o')
    return plot

anim = animation.FuncAnimation(fig, frame, frames=int(t_final/dt), blit=False, repeat=False, interval=1)
plt.show()