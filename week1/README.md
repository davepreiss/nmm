# Maxwell's Demon

This week I wanted to start with a "normal" approach that felt pythonic with a particle class and a bunch of for loops / if statements. Then rewrite it with everything possible vectorized in numpy and see the difference in solve times for each. Below is a 5000 particle simulation with 1000 time steps that took 0.18 s to solve for (and incidentally much longer to write to a .mp4 or .gif file). There's one nice vectorization step where where you can advance each particle by adding the product of velocity and timestep, but then a lot of np.where statements to check for collisions. 

It's kind of hard to see them but on the top left and right of each box is an average velocity measurement for every particle in the respective box. On the bottom left is a time to compute, which is the time to build numpy arrays describing the entire simulation (not including rendering etc).

![](img/animation_fast.gif)

-[Here you can find the fast all numpy code used to make the simulation above.](https://gitlab.cba.mit.edu/davepreiss/nmm/-/blob/a6f0474d8cdb6bec89cc871ceecbd14fa6b71472/week1/maxwells_demon_fast.py)

-[And here you can find the first slower one.](https://gitlab.cba.mit.edu/davepreiss/nmm/-/blob/558752ea110a91f466dedd88306fcb5f8abbc0e9/week1/maxwells_demon.py)

### Numba / Jax

Dissapointingly Jax doesn't have native support for windows, but Numba was easy to get up and running with just a single @jit(nopython=True) decorator. On first run it resulted in a 4.5 s solve time, which is more than an order of magnitude increase. After talking with Erik, it seems like JIT compilation adds a lot of time overhead to pre-compile, so you need to make sure you are at a simulation complexity (particle count or time scale) to catch up to the raw numpy. After this I didn't bother trying trying to run anything on the GPU, but will likely wind up in a place where this is necessary in later weeks.

This was confirmed with the [numba examples](https://numba.pydata.org/numba-doc/dev/user/5minguide.html), specifically the "How to measure the performance of Numba?" example.
