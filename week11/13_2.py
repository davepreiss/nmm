import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

'''
Take as a data set x = {−10,−9,...,9,10}, and y(x) = 0
if x ≤ 0 and y(x) = 1 if x > 0.
'''
def generate_data(x):
    y = np.zeros(len(x))
    y[x > 0] = 1
    return x, y
x,y = generate_data(np.arange(-10, 10, 1))

# A) Fit the data with a polynomial with 5, 10, and 15 terms using a pseudo-inverse of the vandermonde matrix.
def fit_polynomial(x, y, num_coeff):
    # construct the Vandermonde matrix, which is the height of the data and width of the number of coefficients
    Vand = np.vander(x, num_coeff, increasing=True)
    # take the pseudo-inverse of the Vandermonde matrix
    Vand_inv = np.linalg.pinv(Vand)
    # multiply the pseudo-inverse by the y values to get the coefficients
    coefs = Vand_inv.dot(y)
    return coefs

def polynomial_solve(x, coefs):
    result = 0
    for i in range(len(coefs)):
        result += coefs[i] * (x ** i) # sum up the terms in the polynomial
    return result

# '''
plt.plot(x, y, 'o')
for i in range(5, 16, 5):
    coefs = fit_polynomial(x, y, i)
    y_fit = polynomial_solve(x, coefs)
    plt.plot(x, y_fit)
plt.legend(['data', '5', '10', '15'])
plt.title('Vandermond fits with 5, 10, and 15 terms')
plt.show()
# '''

# B) Fit the data with 5, 10, and 15 r^3 RBFs uniformly distributed between x = −10 and x = 10.
def RBF_fit(x, y, num_coeff):
    '''
    For reference: http://www.jessebett.com/Radial-Basis-Function-USRA/
    https://www.youtube.com/watch?v=Kl9xk9BukNE
    '''
    # initialize the centers of the RBFs
    centers = np.linspace(-10, 10, num_coeff)
    # following 14.26
    matrix = np.zeros((len(x),num_coeff))
    for i in range(len(x)):
        for j in range(num_coeff):
            matrix[i][j] = np.abs(x[i] - centers[j]) ** 3
    # take the pseudo-inverse of the matrix
    matrix_inv = np.linalg.pinv(matrix)
    # multiply the pseudo-inverse by the y values to get the coefficients
    coefs = matrix_inv.dot(y)
    return centers, coefs 
    
def RBF_solve(x, centers, coefs):
    result = 0
    for i in range(len(centers)):
        result += coefs[i] * np.abs(x - centers[i]) ** 3
    return result

# '''
plt.plot(x, y, 'o')
for i in range(5, 16, 5):
    centers, coefs = RBF_fit(x, y, i)
    y_fit = RBF_solve(x, centers, coefs)
    plt.plot(x, y_fit)
plt.legend(['data', '5', '10', '15'])
plt.title('RBF fits with 5, 10, and 15 terms')
plt.show()
# '''

# C) Using the coefficients found for these six fits, evaluate the total out-of-sample error at x={−10.5,−9.5,...,9.5,10.5}.
x_half_steps = np.arange(-10.5, 10.5, 1)
truth = np.zeros(len(x_half_steps))
truth[x_half_steps > 0] = 1
# plot the half steps
# plt.plot(x_half_steps, truth, 'o')
# plt.title('Half steps')
# plt.show()
errors_vander = []
errors_RBF = []

for i, rank in enumerate(range(5, 16, 5)):    
    coefs = fit_polynomial(x, y, rank)
    y_fit = polynomial_solve(x_half_steps, coefs)
    error = np.sum(np.abs(y_fit - truth))
    errors_vander.append(error)
    if rank == 15:
        print(np.abs(y_fit - truth))

    centers, coefs = RBF_fit(x, y, rank)
    y_fit = RBF_solve(x_half_steps, centers, coefs)
    error = np.sum(np.abs(y_fit - truth))
    errors_RBF.append(error)

print('Vandermonde errors')
for i, error in enumerate(errors_vander):
    print(f"Rank: {(i+1)*5} Error: {error} ")
print('RBF errors')
for i, error in enumerate(errors_RBF):
    print(f"Rank: {(i+1)*5} Error: {error} ")
