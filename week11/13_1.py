import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

'''
Find the first five diagonal Pade approximants [1/1],..., [5/5] to e^x around the origin. Evaluate the approximations at x=1 and compare with the correct value of e = 2.718281828459045.
How is the error improving with the order? How does that compare to the polynomial error?
https://www.youtube.com/watch?v=3TK8Fi_I0h0
Pade approximants are ratios of polynomials, order is written [N/M] where N is the order of the numerator and M is the order of the denominator
'''
e_truth = 2.718281828459045
taylor_expansion = 

def pade_approx(N, M):
    factorials = np.zeros(N+M+2)
    for i in range(len(factorials)):
        factorials[i] = np.math.factorial(i)