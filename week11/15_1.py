import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

'''
Generate a data set by sampling 100 points from y = tanh(x) between x = -5 to 5,
adding random noise with a magnitude of 0.25 to y
Fit it with a cluster weighted model using a local linear function in the clusters,
y=a+bx. Plot the data and the resulting forecast ⟨y|x⟩, uncertainty ⟨σ2 y|x⟩,
and cluster probabilities p(⃗x|cm)p(cm) as the number of clusters 
is increased (starting with one),and animate their evolution during the EM iterations.
'''

def generate_data(x, sigma):
    noise = np.random.normal(0, sigma, len(x))
    y = np.tanh(x) + noise
    return x, y

x,y = generate_data(np.arange(-5, 5, .1), 0.25)
plt.plot(x, y, 'o')
plt.show()

def cluster_weighted(x, y, num_clusters, num_coeff, iterations):
    N = len(x)
    # For plotting we want to save everything
    forecast = np.zeros(iterations, N)
    forecast_error = np.zeros(iterations, N)
    cluster_prob = np.zeros(iterations, num_clusters)
    mu = np.random.uniform(min(x), max(y), num_clusters)     # initialize the cluster mus
    p_c = np.ones(num_clusters) / num_clusters # initialize the cluster weights 
    variances = np.ones(num_clusters) # initialize the cluster variances
    beta = np.ones((num_clusters, num_coeff)) # betas
    var_y = np.ones(num_clusters) # output variances
    
    for iteration in range(iterations):
        # P(x | c_m), equation (16.24)
        p_x_c = np.zeros((N, num_coeff))
        for i in range(N):
            for m in range(num_coeff):
                p_x_c[i, m] = np.exp((- (X[i] - mu[m]) ** 2) / (2 * variances[m]) ) / np.sqrt(2 * np.pi * variances[m])

    # Save probabilites per cluster
        for m in range(variances):
            for i in range(N):
                cluster_prob[iteration, m, i] = p_x_c[i, m] * p_c[m]

        # (step 16.26) - The mean of our gaussian as a function which we can evaluate at x
        def f(x, coeff):
            s = 0
            for i in range(len(coeff)):
                s += coeff[i] * (x ** i)
            return s
        # P(y | x, c_m) - (equation 16.26)
        p_y_x_c = np.zeros((N, num_coeff))
        for i in range(N):
            for m in range(num_coeff):
                p_y_x_c[i, m] = np.exp((- (y[i] - f(x[i], beta[m])) ** 2) / (2 * var_y[m]) ) / np.sqrt(2 * np.pi * var_y[m])