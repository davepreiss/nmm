import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

'''
Search week
'''

# A) Plot the rosenbrock function in 3D
def rosenbrock_2d(coords):
    x = coords[0]
    y = coords[1]
    return (1 - x) ** 2 + 100 * (y - x ** 2) ** 2

bound = 1
z_bound = 100
x = np.linspace(-bound, bound, z_bound)
y = np.linspace(-bound, bound, z_bound)

X, Y = np.meshgrid(x, y)
Z = rosenbrock_2d([X, Y])

fig = plt.figure()
ax = plt.axes(projection='3d')
ax.plot_surface(X, Y, Z, cmap='viridis', alpha=0.9)

# B) Rosenbrock search with the downhill simplex method
def nelder_mead(obj_func, x_start, step=1, max_iter=1000, tol=0.01, alpha=2.1, gamma=3.9, rho=1.1, sigma=.9):
    step_counter = 0
    eval_counter = 0
    
    # Define the simplex
    n = len(x_start) # Number of dimensions
    simplex = np.zeros((n+1, n)) # Initialize the simplex, which is comprised of 3 XY points (or always 1 more point than the number of dimensions)
    simplex[0] = x_start # Set the starting point as the first point in the simplex
    
    # For every dimension, create a point that is step away from the starting point
    for i in range(n):
        point = x_start.copy()
        point[i] += step
        simplex[i+1] = point

    # Evaluate the objective function for each point in the simplex
    f = np.zeros(n+1)
    for i in range(n+1):
        f[i] = obj_func(simplex[i])
        eval_counter += 1

    simplices = [simplex.copy()]
    simplex_evals = [f.copy()]
    print('Simplex: ', simplex)

    for iteration in range(max_iter):
        step_counter += 1
        # Sort the simplex by function value
        order = np.argsort(f)
        simplex = simplex[order]
        f = f[order]
        
        # Calculate the centroid
        centroid_coords = np.mean(simplex[:-1], axis=0)
        print(centroid_coords)
        
        # Reflection
        x_r = centroid_coords + alpha*(centroid_coords - simplex[-1])
        f_r = obj_func(x_r)
        eval_counter += 1
        if f[0] <= f_r < f[-2]:
            simplex[-1] = x_r
            f[-1] = f_r
        # Expansion
        elif f_r < f[0]:
            x_e = centroid_coords + gamma*(x_r - centroid_coords)
            f_e = obj_func(x_e)
            eval_counter += 1
            if f_e < f_r:
                simplex[-1] = x_e
                f[-1] = f_e
            else:
                simplex[-1] = x_r
                f[-1] = f_r
        # Contraction
        else:
            centroid_coordsc = centroid_coords + rho*(simplex[-1] - centroid_coords)
            f_cc = obj_func(centroid_coordsc)
            eval_counter += 1
            if f_cc < f[-1]:
                simplex[-1] = centroid_coordsc
                f[-1] = f_cc
            # Shrink
            else:
                for i in range(1, n+1):
                    simplex[i] = simplex[0] + sigma*(simplex[i] - simplex[0])
                    f[i] = obj_func(simplex[i])
                    eval_counter += 1
        # After each iteration, add the simplex to the list of simplices
        simplices.append(simplex.copy())

        # Check the stopping criterion, if our simplex is small enough, return the minimum point
        if np.max(np.abs(simplex[1:] - simplex[0])) < tol and np.max(np.abs(f[1:]-f[0])) < tol:
            simplices.append(simplex.copy())
            return simplices, step_counter, eval_counter
    
    # If we reach the maximum number of iterations (the for loop expires), return the list of simplices
    return simplices, step_counter, eval_counter

# Downhill simplex method
x_start = np.array([-1, -1])
stopping_criteria = 1e-6

simplex_points, steps, evals = nelder_mead(rosenbrock_2d, x_start)
final_point = simplex_points[-1][-1]
print('Final Point: ', final_point)
print('Iterations: ', steps)
print('Evaluations: ', evals)

# Each simplex consists of 3 points, so we need to reshape the array
simplex_points = np.array(simplex_points).reshape(-1, 2)

all_evals = np.zeros(len(simplex_points))
for i in range(len(simplex_points)):
    all_evals[i] = rosenbrock_2d(simplex_points[i])

# Plot all of the simplex evaluations
# Add iterations and evaluations to the plot
ax.text2D(0.05, 0.95, "Iterations: %d" % steps, transform=ax.transAxes)
ax.text2D(0.05, 0.90, "Evaluations: %d" % evals, transform=ax.transAxes)
ax.view_init(30, 45)
# Plot lines connecting the simplex points
for i in range(len(simplex_points)-1):
    ax.plot(simplex_points[i:i+2,0], simplex_points[i:i+2,1], all_evals[i:i+2]+.01, color='red', linewidth=1, marker = 'o', markersize=3)
plt.show()