import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from cmaes import CMA

'''
Search week
'''

# A) Plot the rosenbrock function in 3D
def rosenbrock_2d(coords):
    x = coords[0]
    y = coords[1]
    return  (1 - x) ** 2 + 100 * (y - x ** 2) ** 2 # + np.sin(x)/x

def parabola(coords):
    x = coords[0]
    y = coords[1]
    return x**2 + y**2

bound = 2
z_bound = 100
x = np.linspace(-bound, bound, z_bound)
y = np.linspace(-bound, bound, z_bound)

X, Y = np.meshgrid(x, y)
Z = rosenbrock_2d([X, Y])

fig = plt.figure()
ax = plt.axes(projection='3d')
# ax.plot_surface(X, Y, Z, cmap='viridis', alpha=.5)

# F) Rosenbrock search with CMAES
'''
For reference: https://www.youtube.com/watch?v=lj_qdRxzjMo
'''

optimizer = CMA(mean=np.array([-.5,-.5]), sigma=.5, population_size=20)
print(optimizer.ask())

starting_point = [-2, -2]
generations = 50
sqrt = int(np.sqrt(generations))
points = np.ndarray((generations, optimizer.population_size, 3))
generation_counter = 0

for generation in range(generations):
    generation_counter += 1
    solutions = []
    for i in range(optimizer.population_size):
        point = optimizer.ask()
        value = rosenbrock_2d([point[0], point[1]])
        solutions.append((point, value))
        points[generation,i] = point[0], point[1], value
    optimizer.tell(solutions)

# Animate each generation
def animate(i):
    ax.clear()
    ax.view_init(30, 45)
    ax.text2D(0.45, 0.95, "Generation: " + str(i), transform=ax.transAxes)
    plt.title("Parabolic function with CMA-ES")
    ax.plot_surface(X, Y, Z, cmap='viridis', alpha=.5)
    ax.scatter(*zip(*points[i]), c = plt.cm.jet(i/generations))

anim = animation.FuncAnimation(fig, animate, frames=generations, interval=100, repeat=False)
plt.show()
# save animation
# anim.save('cmaes_rosenbrock_2d.gif', writer='imagemagick', fps=4)