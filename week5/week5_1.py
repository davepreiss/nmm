import numpy as np
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.animation import FuncAnimation, writers

'''
6.1)
The wave equation suggests that where you have a large curvature (rate of change of the slope) you will have a large change with respect to time, proportional to c.
https://www.youtube.com/watch?v=lHGMjfCPZik
Recusion relation taken from here: https://wiki.seg.org/wiki/Solving_the_wave_equation_in_1D
y(t+dt) = 2*y(t) - y(t-dt) + r*(y_right - 2*y_self + y_left)
where r = (c*dt/dx)^2
'''

class tensioned_string():

    def __init__(self, x, y0, c):
        self.x, self.y, self.y0, self.c = np.copy(x), self.pad_array(y0), self.pad_array(y0), c
        self.y_prev = np.copy(self.y0)
    
    def pad_array(self, arr): # pads the array with an additional first and last element, which will allow us to convolve the edges (these will share the boundary condition)
        return np.concatenate((arr[:1], arr, arr[-1:]))
    
    def increment(self, dt):
        ''' increment by dt '''
        r = (self.c * dt / dx)**2 # you can use np.gradient for non-constant dx
        temp = np.copy(self.y)
        # now apply the recursion relation
        self.y[1:-1] = 2*self.y[1:-1] - self.y_prev[1:-1] + r*(self.y[2:] - 2*self.y[1:-1] + self.y[:-2])
        # with damping (assume dt is built into gamma because it was going unstable)
        # self.y[1:-1] -= 0.005*(self.y[1:-1] - temp[1:-1])
        self.y_prev = temp

        # boundary condition of clamped end points. We accept that they've changed above, but now we will just reset them to their original values
        self.y[[0,1,-2,-2]] = self.y0[[0,1,-2,-2]]

L = 100
dx = 0.5
x = np.linspace(0, 100, int(L/dx)) # string is 100 units long, with 128 points
y = np.empty_like(x)

d0 = 0.1 # initial displacement of the string

# Initial Conditions - sinusoidal ripple
for i in range(len(x)):
    y[i] = d0*(np.sin((2*np.pi)*x[i]/10) / (10 + (x[i] - 10)**2))

# Initial Conditions - plucked string
# d0_location = 0.7 # % where along the string the initial displacement occurs
# y[64] = d0
# set everything between our "pluck" location to be linear to that point
# y[np.where(x <= d0_location*x[-1])] = d0/(d0_location*x[-1]) * x[np.where(x <= d0_location*x[-1])]
# y[np.where(x > d0_location*x[-1])] = -d0/((1.0 - d0_location)*x[-1]) * (x[np.where(x > d0_location*x[-1])] - x[-1])

c = 5 # speed of waves
dt = 0.01   # time step
total_time = 300
fps = 20

string_1 = tensioned_string(x, y, c)

fig,ax = plt.subplots()
ax.plot(x, y)
line, = ax.plot(x,y)
ax.set_ylim([-1.5*max(y),1.5*max(y)]) # set our axes limits to be slighty past the intial displacement
 
def init():
    return line,

def update(i):
    string_1.increment(dt)
    line.set_ydata(string_1.y[1:-1])  # update the data
    return line,

anim = animation.FuncAnimation(fig, update, init_func=init, frames=int(600), blit=True, interval = 0.05, repeat=True)
# plt.show()

my_dpi = 96
f = r"c://Users/david/Desktop/NMM/week5/img/wave_no_damp.gif" 
fps2 = 20
writervideo = animation.FFMpegWriter(fps=fps2, extra_args=['-vcodec', 'libx264']) 
anim.save(f, writer='imagemagick', fps=fps2, dpi=my_dpi)