import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
import matplotlib.animation as animation
from matplotlib import cm

'''
6.4)
Use SOR to solve Laplace's equation in 2D, with boundary conditions
uj,1 = u1,k = 0, uN,k = -1,uj,N = 1
and explore how the convergence rate depends on alpha, and how the best choice for alpha depends on the lattice size.

Equation 9.69 with rho = 0 (forcing function?), therefore our last term is zero
'''

# SOR = Successive Over-Relaxation -- https://en.wikipedia.org/wiki/Successive_over-relaxation

def SOR_step(u):
    # we are literally just going to implement equation 9.69 here, starting with the first term
    new_u = (1 - alpha) * u
    # enforce boundary or initial conditions
    new_u[0, :],  new_u[:, 0], new_u[-1, :], new_u[:, -1] = 0, 0, 1, -1
    # new_u[lattice_size//2, lattice_size//2] = 1 # set the middle point to 1


    # Do it with a scipy convolution for speed
    filt = np.array([[ 0, alpha/4,  0],
                     [alpha/4, 0, alpha/4],
                     [ 0, alpha/4, 0]])
    new_u = signal.convolve2d(u, filt, boundary='fill', mode='same') # convolve it up
    new_u[0, :],  new_u[:, 0], new_u[-1, :], new_u[:, -1] = 0, 0, 1, -1
    # new_u[lattice_size // 2, lattice_size //2] = 1 
    return new_u

lattice_size = 50
total_time = 100
dt = 0.05
alpha = 1
fps = 30

u = np.zeros((lattice_size, lattice_size))

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

xs = np.linspace(-1, 1, len(u))
ys = np.linspace(-1, 1, len(u))
X, Y = np.meshgrid(xs, ys)
u = SOR_step(u)

surf = ax.plot_surface(X, Y, u, rstride=2, cstride=2 ,cmap=cm.jet)    # plot a 3d surface plot

def update(i):
    global u
    u = SOR_step(u)
    ax.cla() # fairly certain that ax.cla() is very slow, but can't find the update function
    ax.plot_surface(X, Y, u, cmap = 'viridis', edgecolor = 'none')

anim = animation.FuncAnimation(fig, update, total_time, interval=1000/fps, blit = False)
plt.show()

# f = r"c://Users/david/Desktop/NMM/week5/img/laplace_2D_edges.gif" 
# writervideo = animation.FFMpegWriter(fps=fps/2, extra_args=['-vcodec', 'libx264']) 
# anim.save(f, writer='imagemagick', fps=fps/3, dpi=96)
