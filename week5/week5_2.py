import numpy as np
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt
import matplotlib.animation as animation

'''
6.2) (Main Question)
Write a program to solve a 1D diffusion problem on a lattice of 500 sites, with an initial condition of zero
at all the sites, except the central site which starts at the value 1.0. Take D = ∆x = 1, and use fixed boundary conditions set equal to zero.
D = ∆x = 1
'''

lattice_num = 500
total_time = 50
dt = 0.1

state = np.zeros(lattice_num)
state[lattice_num // 2] = 1 # set the middle site to 1, // rounds down
all_states = np.zeros((int(total_time/dt),lattice_num))
all_states[0] = np.copy(state)
diffusion = 1
dx = 1

def time_step(last_state, diffusion):
    new_state = np.copy(last_state) # deep copy the last state
    alpha = diffusion * dt / (dx ** 2)  # alpha is the potentially time/space dependent diffusion constant
    filt = np.array([alpha, 1 - 2*alpha, alpha]) # convolution filter for diffusion
    new_state[1:-1] = np.convolve(last_state, filt, mode='valid')   # convolution
    return new_state

for t in range(total_time):
    state = time_step(state, diffusion)
    # input()
    # print(state)
    all_states[t] = np.copy(state)

my_dpi = 96
fig = plt.figure(figsize=(400/my_dpi, 400/my_dpi), dpi=my_dpi)
ax = fig.add_subplot(1, 1, 1)
ax.set_xlim(0, lattice_num)
ax.set_ylim(-0.5, 1)
ax.set_title(f'dt: {dt: .2f} s', fontdict=None)
ax.grid()

x = np.arange(0,lattice_num)
plot = ax.plot(x, all_states[0])

def animate(i):
    plot[0].set_data(x, all_states[i])
    return plot

anim = animation.FuncAnimation(fig, animate, range(total_time), blit=False, interval=50, repeat=True)
plt.show()

# f = r"c://Users/david/Desktop/NMM/week5/img/dt1_0.gif" 
# writervideo = animation.FFMpegWriter(fps=30, extra_args=['-vcodec', 'libx264']) 
# anim.save(f, writer='imagemagick', fps=30, dpi=my_dpi)

'''
6.2) A
Use the explicit finite difference scheme, and look at the behavior for ∆t = 1, 0.5, and 0.1.
What step size is required by the Courant condition?

See the 3 gifs at each dt, which show the "boom" at dt=1, the start of stability at dt=0.5, and the acceptable solution at dt=0.1.
Courant condition: dt <= dx^2 / (2*diffusion)
See answer of 0.5 below:
'''

dt_max = dx**2 / (2*diffusion)
print(dt_max)