import numpy as np
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt
import matplotlib.animation as animation

'''
6.3)
Use ADI to solve a 2D diffusion problem on a lattice, starting with randomly seeded values.
'''

# ADI = Alternating Direction Implicit -- https://en.wikipedia.org/wiki/Alternating-direction_implicit_method
lattice_x_num = 50
lattice_y_num = 50
total_time = 100
dt = 0.1

state = np.zeros((lattice_y_num, lattice_x_num))
state[lattice_y_num // 2][lattice_x_num // 2] = 1 # set the middle site to 1, // rounds down
all_states = np.zeros((int(total_time/dt),lattice_num))
all_states[0] = np.copy(state)
diffusion = 1
dx = 1