import numpy as np
import matplotlib.pyplot as plt

'''
6.2)
For a simple harmonic oscillator y'' + y = 0, with initial conditions y(0) = 1, y'(0) = 0,
find y(t) from t = 0 to 100π. Use an Euler method and a fixed-step fourth-order Runge-Kutta method.
For each method check how the average local error, and the error in the final value and slope, depend on the step size
'''

# Euler method
def euler_integrate(step):
    t = np.arange(0, 100*np.pi, step)
    increments = np.arange(1, len(t), 1) # we will need something indexed from 1 to final time_step to not interfere with the initial conditions
    y = np.zeros(len(t))
    y[0] = 1
    dy = np.zeros(len(t))
    dy[0] = 0
    for time in increments:
        y[time] = y[time-1] + step*dy[time-1]
        dy[time] = dy[time-1] - step*(y[time-1])
        # dy[time] = dy[time-1] - step*(y[time]) # reverse euler? Just use the expected new y 
    return y, dy, t

# Runge-Kutta method
def rk4_integrate(step):
    t = np.arange(0, 100*np.pi, step)
    increments = np.arange(1, len(t), 1) # we will need something indexed from 1 to final time_step to not interfere with the initial conditions
    y = np.zeros(len(t))
    y[0] = 1
    dy = np.zeros(len(t))
    dy[0] = 0
    for time in increments:
        k1, k2, k3, k4 = [0, 0], [0, 0], [0, 0], [0, 0] # make position and velocity vectors
        
        k1[0] = step * dy[time-1] # normal euler step
        k1[1] = - step * y[time-1]
        k2[0] = step * (dy[time-1] + k1[1]*0.5) # half euler step in the direction of k1
        k2[1] = -step * (y[time-1] + k1[0]*0.5)
        k3[0] = step * (dy[time-1] + k2[1]*0.5) # half euler step in the direction of k2
        k3[1] = -step * (y[time-1] + k2[0]*0.5)
        k4[0] = step * (dy[time-1] + k3[1]) # full euler step in the direction of k3
        k4[1] = -step * (y[time-1] + k3[0])

        y[time] = y[time-1] + (k1[0] + 2* k2[0] + 2 * k3[0] + k4[0])/6 # divide by 6 (not 4) because we assigned 2x value to k2 and k3
        dy[time] = dy[time-1] + (k1[1] + 2* k2[1] + 2 * k3[1] + k4[1])/6
    return y, dy, t

step1 = 0.001
step2 = 0.01

# y1, dy1, t1 = euler_integrate(step1)
# y2, dy2, t2 = euler_integrate(step2)

y1, dy1, t1 = rk4_integrate(step1)
y2, dy2, t2 = rk4_integrate(step2)

fig, ax = plt.subplots(2)
ax[0].plot(t1, y1)
ax[0].title.set_text('Times step = ' + str(step1) + '\n Final y: ' + str(round(y1[-1],3)) + '\n Final dy: ' + str(round(dy1[-1],3)))
ax[1].plot(t2, y2)
ax[1].title.set_text('Times step = ' + str(step2) + '\n Final y: ' + str(round(y2[-1],3)) + '\n Final dy: ' + str(round(dy2[-1],3)))
plt.tight_layout()
plt.show()