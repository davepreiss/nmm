import numpy as np
import matplotlib.pyplot as plt

'''
6.4)
Numerically solve the differential equation found in Problem 4.3:
l*ddθ + (g + ddz)*sinθ = 0
Take the motion of the platform to be periodic, and interactively explore the dynamics of
the pendulum as a function of the amplitude and frequency of the excitation.

Consider a pendulum with a mass m connected by a massless rod of length l to a moveable platform at height z (Figure 6.4)
'''
step = 0.001
t_final = 10*np.pi
t = np.arange(0, t_final, step)
increments = np.arange(1, len(t), 1)
time = 0

# Platform Parameters (periodic motion and very differentiable)
A = 1 # amplitude of the platform's motion [m]
platform_omega = 1 # angular frequency of the platform's motion [rad/s]
zs = A*np.sin(platform_omega*t) # all platform positions as a function of time
ddzs = A*platform_omega**2*np.cos(platform_omega*t) # all platform accelerations as a function of time

# Pendulum Parameters
thetas = np.zeros(len(t))
d_thetas = np.zeros(len(t))
dd_thetas = np.zeros(len(t))
l = 1 # length of pendulum
g = 9.81 # acceleration due to gravity
theta_0 = 0 # initial angle of the pendulum [rad]
d_theta_0 = 1 # initial angular velocity of the pendulum [rad/s]

def d_theta(theta, omega, t):
    return omega

def dd_theta(theta, omega, t):
    solution = -np.sin(theta)/l * (g - ddzs[int(t/step)])
    print(solution)
    return solution

def rk4_step(prev_values, dif_eqs, step, time):
    variables = np.array(prev_values)
    k1, k2, k3, k4 = [0, 0], [0, 0], [0, 0], [0, 0] # make position and velocity vector
    ddy = dif_eqs[1](prev_values[0], prev_values[1], time)
    dy = dif_eqs[0](prev_values[0], prev_values[1], time)
    print(ddy)
    k1[0] = step * ddy # normal euler step for velocity
    k1[1] = - step * dy  # normal euler step for position
    k2[0] = step * (ddy + k1[1]*0.5) # half euler step in the direction of k1
    k2[1] = -step * (dy + k1[0]*0.5)
    k3[0] = step * (ddy + k2[1]*0.5) # half euler step in the direction of k2
    k3[1] = -step * (dy + k2[0]*0.5)
    k4[0] = step * (ddy + k3[1]) # full euler step in the direction of k3
    k4[1] = -step * (dy + k3[0])
    variables = variables + np.array(k1)/6 + np.array(k2)/3 + np.array(k3)/3 + np.array(k4)/6
    return variables

for increment in increments:
    time = t[increment]
    thetas[increment], d_theta[increment] = rk4_step([thetas[increment-1], d_thetas[increment-1]], [d_theta, dd_theta], step, time)

# let's plot the chaotic theta over the periodic z
plt.plot(t, zs)
plt.plot(t, thetas)