import numpy as np
import matplotlib.pyplot as plt

def generate_data(size, sigma):
    # Generate 100 points uniformly distributed between 0 and 1
    x = np.random.rand(100)
    # Ley y - 2+3x + C, where C is a gaussian random variable with standard deviation of 0.5
    C = np.random.normal(0, sigma, size)
    y = 2 + 3*x + C
    return x, y

# Use an SVD to fit y = a + bx to this data set, finding a and b. 
# https://ltetrel.github.io/data-science/2018/06/08/line_svd.html


def fit_line(x, y):
    # Make a matrix A with the x values in the first column and 1's in the second column
    A = np.vstack([x, y]).T
    avg = np.mean(A, axis = 0)
    u, s, V = np.linalg.svd(A - avg) # V is a 2x2 matrix with the eigenvectors as columns, with leftmost columns providing the best fit
    # get a rise over run slope value from the direction vector V
    slope = V[0,1]/V[0,0]
    # find the y intercept by plugging in the x value of the average
    intercept = avg[1] - slope*avg[0]
    return slope, intercept, V, s

# Generate the data
size = 100
sigma = 0.5
x, y = generate_data(size, sigma)
slope, intercept, V, s = fit_line(x, y)

slope = round(slope, 3)
intercept = round(intercept, 3)
print("Fit line: y = {0} + {1}x".format(intercept, slope))

# Plot the data and the fit
plt.title("Fit line: y = {0} + {1}x".format(intercept, slope))
plt.plot(x, y, 'o', label='Original data', markersize=3)
plt.plot(x, slope*x + intercept, 'r', label='Fitted line')
# set the limits of the plot
plt.show()

'''
Evaluate the errors in a and b
'''

# Part A) According to Equation 12.34
errors = []
for i in range(2):
    error = 0
    for j in range(2):
        error += (V[j, i] / s[j]) ** 2
    errors.append(error)
print("Known error: slope error: %.4f" % np.sqrt(errors[0] * (sigma ** 2)))
print("Known error: intercept error: %.4f" % np.sqrt(errors[1] * (sigma ** 2)))

# Part B) Bootstrap Sampling
space = np.linspace(0, 99, 100) # set up a linespace for each x value
A, B = [], []
for i in range(100):
    sample = np.random.choice(space, 100)
    x_b, y_b = x[sample.astype(int)], y[sample.astype(int)]
    a, b = np.polyfit(x_b, y_b, 1)
    A.append(a)
    B.append(b)
print("Bootstrap: slope error: %.4f" % np.var(A))
print("Bootstrap: intercept error: %.4f" % np.var(B))

# Part C) From fitting an ensemble of 100 independent data sets
# Here we are literaly just going to run the same code as part B 100 times
# and then take the variance of the results
A, B = [], []
for i in range(100):
    x, y = generate_data(size, sigma)
    a,b,V,s = fit_line(x, y)
    A.append(a)
    B.append(b)
print("Ensemble: slope error: %.4f" % np.var(A))
print("Ensemble: intercept error: %.4f" % np.var(B))