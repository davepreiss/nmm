import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

'''
Generate 100 points x uniformly distributed between 0 and 1, and let y = sin(2+ 3x) + ζ, where ζ is a Gaussian random variable
with a standard deviation of 0.1. Write a Levenberg-Marquardt routine to fit y = sin(a+bx) to this data set starting from a = b = 1 
(remembering that the second-derivative term can be dropped in the Hessian), and investigate the convergence for both fixed and adaptively 
adjusted λ values.
'''

def generate_data(size, sigma):
    # Generate 100 points uniformly distributed between 0 and 1
    x = np.random.rand(100)
    # Ley y - 2+3x + C, where C is a gaussian random variable with standard deviation of 0.5
    C = np.random.normal(0, sigma, size)
    y = np.sin(2 + 3*x) + C
    return x, y

# Levenberg-Marquardt routine to fit y = sin(a+bx) to this data set starting from a = b = 1
def levenberg_step(x, y, a, b, l):
    # every noisy y - every y from our fit:
    error = (y - np.sin(a + b*x))**2
    error_sum = np.sum(error)
    J = np.zeros(2)
    J[0] = -2 * np.sum((y - np.sin(a + b*x)) * np.cos(a + b * x))    # from wolfram
    J[1] = -2 * np.sum((y - np.sin(a + b*x)) * np.cos(a + b * x) * x)

    # Hessian from wolfram and then using 12.28 to form M...
    M = np.zeros((2, 2))
    M[0, 0] = 0.5 * (1 + l) * 2 * np.sum(np.cos(a + b * x) ** 2)
    M[1, 1] = 0.5 * (1 + l) * 2 * np.sum((x * np.cos(a + b * x)) ** 2)

    M[0, 1] = 0.5 * 2 * np.sum(x * (np.cos(a + b * x) ** 2)) 
    M[1, 0] = M[0, 1] # it's a symmetric matrix

    M_inv = np.linalg.inv(M)
    step = -M_inv.dot(J)
    return step

a = 1
b = 1
l = 1
steps = 50

size = 100
sigma = 0.1
x, y = generate_data(size, sigma)

A = []
B = []

for i in range(steps):
    coefficients = levenberg_step(x, y, a, b, l)
    A.append(a)
    B.append(b)
    a += coefficients[0]  # we are taking little relative moves of a and b, not returning absolute positions
    b += coefficients[1]
    # print("a: %.4f, b: %.4f" % (a, b))

print(a,b)
# '''
# Here we plot the data and the fit line
fig, (ax1, ax2) = plt.subplots(2, 1)
# slim plot
fig.subplots_adjust(hspace=0.5)
ax1.plot(x, y, 'o')
x_sorted = np.sort(x)
ax1.plot(x_sorted, np.sin(a + b*x_sorted))
# print inital parameters
ax1.text(0,-1, 'Fit line: y = sin(a+bx)\ny = sin(%.2f + %.2fx)' % (a, b), fontsize=10)
ax1.set_title('Levenberg-Marquardt Fit\nsteps = %.2f, sigma = %.2f' % (steps, sigma))
# Plot A and B as an XY scatter plot with color associated with the value of the iteration number
ax2.scatter(A, B, c=range(steps))
ax2.set_title('Converging a and b values')
plt.show()
# '''

'''
# Here we animate the fit converging over the scatter plot
my_dpi = 96
fig = plt.figure(figsize=(800/my_dpi, 400/my_dpi), dpi=my_dpi)
ax = fig.add_subplot(111)
# Scatter plot
scatter = ax.plot(x, y, 'o')
# Fit line
x_sorted = np.sort(x)
fit = ax.plot(x_sorted, np.sin(a + b*x_sorted))

# Animate the fit converging over the scatter plot
def animate(w):
    global A, B
    x = A[w]
    y = B[w]
    fit[0].set_data(x_sorted, np.sin(x + y*x_sorted))

anim = animation.FuncAnimation(fig, animate, frames=int(50))
# plt.show()

path = r"c://Users/david/Desktop/NMM/week9/img/animation.gif" 
# save as a gif
anim.save(path, writer='imagemagick', fps=8, dpi=my_dpi)
'''