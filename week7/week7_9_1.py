import scipy.spatial as spatial
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

def calc_force(P1,P2):
    # calculate the force between points P1 and P2 (both 2D numpy arrays)
    # the neutral distance is the variable neutral_distance
    dist = np.linalg.norm(P1 - P2) # we should have this from scipy already but recalculating for now...
    return spring_constant * (dist - neutral_distance) * (P1 - P2) / dist

def solve_forces(particles, velocities):
    particles_tree = spatial.cKDTree(particles)
    # For each point, calculate the force due to the nearest neighbors
    F = np.zeros((x_num*y_num, 2))
    for particle in range(x_num*y_num):
        # return the index of all particles within radius of radius_check
        neighbors = particles_tree.query_ball_point(particles[particle, :], radius_check)
        # calculate the force on the particle due to the nearest neighbor
        for neighbor in neighbors:
            if neighbor != particle:
                F[particle, :] -= calc_force(particles[particle, :], particles[neighbor, :])
    F_damping = damping_constant * velocities  # damping forces
    # add damping forces to the total force, ensuring it can't reverse the direction of the force
    F[:, 0] -= np.sign(velocities[:, 0]) * np.minimum(np.abs(F_damping[:, 0]), np.abs(F[:, 0]))
    F[:, 1] -= np.sign(velocities[:, 1]) * np.minimum(np.abs(F_damping[:, 1]), np.abs(F[:, 1]))
    F[:, 1] -= gravity  # gravity
    return F

def solve_velocities(F, V_prev):
    # update the velocity of each particle from V_prev according to the force acting on it
    V = V_prev + dt * F / particle_mass
    return V

def solve_positions(particles, V):
    # update the position of each particle from particles according to the velocity acting on it
    particles_new = particles + dt * V
    # CANTILEVERED BEAM - set all particles in the first column to have a position of zero
    for particle_num in range(len(particles)):
        # if the particle is in the first row:
        if particle_num <= y_num:
            # keep that particle in place
            particles_new[particle_num, 0] = particles[particle_num, 0]
            particles_new[particle_num, 1] = particles[particle_num, 1]
    return particles_new

def best_fit(particles, order):
    a, b = np.polyfit(particles[:, 0], particles[:, 1], deg = order)
    return np.array([a, b])

# Constants:
x_num = 100
y_num = 30
t_final = 1.5
dt = 0.01
mesh_spacing = 1
neutral_distance = mesh_spacing
radius_check = mesh_spacing*1.5
spring_constant = 1000
damping_constant = 10
gravity = 0.05
particle_mass = 1
height_offset = 8

sub_increments = 10

# Create our triangular mesh of points
particles = np.zeros((x_num*y_num, 2))
velocities = np.zeros((x_num*y_num, 2))
forces = np.zeros((x_num*y_num, 2))

# create a mesh of particles, indexes start at point 0,0 and grow up the y-axis, then move to the next column and grow up that one
for i in range(x_num):
    for j in range(y_num):
        particles[i*y_num+j, 0] = i
        particles[i*y_num+j, 1] = j * np.sqrt(3) / 2 # height of equilateral triangle
        if j % 2 == 1:  # add an offset to every other point
            particles[i*y_num+j, 0] += 0.5

# particles[-1, 0] -= mesh_spacing*.5 # offset a particle for chaos
particles *= mesh_spacing # scale the mesh to the desired spacing
particles[:,1] += mesh_spacing*height_offset

# Create a scatterplot of X
my_dpi = 96
fig = plt.figure(figsize=(800/my_dpi, 400/my_dpi), dpi=my_dpi)
ax = fig.add_axes([0, 0, 1, 1], frameon=False)
# numpy array of colors for each particle in shades from red to blue
colors = np.zeros((x_num*y_num, 4))
colors[:, 0] = np.linspace(1, 0, x_num*y_num)
colors[:, 2] = np.linspace(0, 1, x_num*y_num)
colors[:, 3] = 1
plt.axis('equal')   # ensure axes are equally scaled
plt.plot([-mesh_spacing*3, x_num*mesh_spacing+mesh_spacing*3], [0, 0], color = 'k') # Draw a line representing the ground at y = 0;
best_fit_0 = best_fit(particles, 1)
static_line = ax.plot([0, x_num*mesh_spacing], [best_fit_0[0]*0 + best_fit_0[1], best_fit_0[0]*x_num*mesh_spacing + best_fit_0[1]], color='grey', linestyle='--', linewidth=1)
fit_line = ax.plot([0, x_num*mesh_spacing], [best_fit_0[0]*0 + best_fit_0[1], best_fit_0[0]*x_num*mesh_spacing + best_fit_0[1]], color='black', linestyle='--', linewidth=1)
plot = ax.scatter(particles[:, 0], particles[:, 1],
                  marker = 'o',
                  s = 20,
                  color = colors,
)
# Optionally plot the intial conditions
# plt.scatter(particles[:, 0], particles[:, 1], marker = 'o', s = 10, color = 'b')

def animate(w):
    global particles, velocities, forces
    for i in range(sub_increments):
        forces = solve_forces(particles, velocities)
        velocities = solve_velocities(forces, velocities)
        particles_new = solve_positions(particles, velocities)
        x = particles_new[:, 0]
        y = particles_new[:, 1]
        particles = particles_new  
    plot.set_offsets(np.c_[x,y])
    best_fit_new = best_fit(particles, 1)
    # update fit_line with the new best fit data
    fit_line[0].set_data([0, x_num*mesh_spacing], [best_fit_new[0]*0 + best_fit_new[1], best_fit_new[0]*x_num*mesh_spacing + best_fit_new[1]])
    # plt.plot([0, x_num*mesh_spacing], , color='steelblue', linestyle='--', linewidth=1)

anim = animation.FuncAnimation(fig, animate, frames=int(t_final/dt))
# plt.show()

import matplotlib as mpl 
mpl.rcParams['animation.ffmpeg_path'] = r'C:\ffmpeg_2023_03_30\bin\ffmpeg.exe'
path = r"c://Users/david/Desktop/NMM/week7/figures/animation.mp4" 
writervideo = animation.FFMpegWriter(fps=30, extra_args=['-vcodec', 'libx264']) 
anim.save(path, writer=writervideo, dpi=my_dpi)