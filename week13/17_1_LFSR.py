import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

from pylfsr import LFSR
import torch
# print(torch.cuda.is_available())
from torch import nn
from torch.optim import Adam
from torch import Tensor

'''
17_1:
Train a multi-layer perceptron to predict the output of an order 10 maximal length linear feedback shift register 
from the preceding 10 values, then generate new data by feeding the output back to the input and compare the generated and correct sequences.

This XOR example was a helpful reference:
https://www.youtube.com/watch?v=3I_66lyFOqI
'''

def create_data(state_0 = [1,0,0,0,0,0,0,0,0,0]):
    L = LFSR(initstate = state_0, verbose = False)
    x = []
    y = []
    for i in range(2**10):
        x.append(L.state)
        y.append([L.outbit])
        L.next()
    x = np.array(x, dtype = np.float32)
    y = np.array(y, dtype = np.float32)
    # override the first outbit which is -1
    y[0] = [0]
    return x, y

class LFSRModel(nn.Module):
    def __init__(self):
        super().__init__()
        self.layers = nn.Sequential(
            nn.Linear(10, 10), # 2nd ten can be any number mapping the input size to some space 10x10 has 100 edges/numbers/weights
            # linear is all just multiplictions of weights and inputs
            nn.Sigmoid(), # sigmoid (or relu) creates a non-linearity threshold
            nn.Linear(10, 10), # number of inputs, must be number of outputs, here we map all of the previous outputs to a single output
            nn.Sigmoid(),
            nn.Linear(10, 1), # number of inputs, must be number of outputs, here we map all of the previous outputs to a single output
            nn.Sigmoid(),
        )
        self.optimizer = Adam(self.parameters()) # after you have the gradient, now we have to choose how to update the weights
        self.loss = nn.MSELoss() # how we compare the predicted and actual values
    
    def forward(self, X):
        return self.layers(X)
    
    def fit(self, X, y_true):
        self.optimizer.zero_grad() # zero the gradients
        y_pred = self.forward(X) # feed the network with an input
        loss = self.loss(y_true, y_pred) # then calculate the loss (error between prediction and groudn truth)
        loss.backward() # backwards calculates the gradients over every weight
        self.optimizer.step() # then updates the weights
        return loss.item()


# Initialize the data and model
xs, ys = create_data()
# create a copy of the data
real_solution = ys.copy()
xs = Tensor(xs)
ys = Tensor(ys)
print(xs.shape)
print(ys.shape)
lfsr_model = LFSRModel()
# print(lfsr_model(xs))

# Now train the model, printing every mod epochs to get an idea of convergence
EPOCHS = 5000
for epoch in range(EPOCHS):
    loss = lfsr_model.fit(xs, ys)
    if epoch % 1000 == 0:
        print(loss)

# Now predict again
new_model = lfsr_model(xs)
new_model = np.round(new_model.detach().numpy()) # round the values to 0 or 1

# return all indeces where real_sum and predicted_sum are different
# this is where the model is wrong
wrong = np.where(new_model != real_solution)[0]
# fprint
print("Number of wrong predictions: " + str(len(wrong)))
print("Indeces where solution and predicted differ: " + str(wrong))
print("Inputs with incorrect outputs: " + str(xs[wrong]))
print("Predicted Values: " + str(new_model[wrong]))
print("Solution Values: " + str(real_solution[wrong]))